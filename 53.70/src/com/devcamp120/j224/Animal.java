package com.devcamp120.j224;

public abstract class Animal {
    protected int age ;
    protected String gender ;

    // có abstract trước public thì ko có nội dung trong hàm 
    abstract public void SoundAnimal();
    abstract public void EatAnimal();
    abstract public String toString();
        
}
