package com.devcamp120.j224;

    public class Duck extends Animal {
       private String breakColor ;

    public Duck (int age , String gender , String breakColor){
        this.age = age;
        this.gender = gender ;
        this.breakColor = breakColor;
    }

    @Override
    public String toString(){
        return "Duck {\"gender\" : " + this.gender+ ", age : " + age + " , breakColor : "+ this.breakColor + "" + "}";
    }

    @Override
    public void SoundAnimal() {
        System.out.println("Duck sound");
    }
    @Override
    public void EatAnimal() {
        System.out.println("Duck eat anything");
    }
}