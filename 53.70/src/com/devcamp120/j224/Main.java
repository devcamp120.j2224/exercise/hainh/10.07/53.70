package com.devcamp120.j224;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // tính đa hình trong java
        Animal duck = new Duck(3 , "Vịt Cái" , "màu Vờn") ;
        Animal fish = new Fish( 2 , "Cá Vờn" , 4 , true) ;
        Animal zebra = new Zebra(1 , "Ngựa Vằn Đực" , true);

        ArrayList<Animal> animalist = new ArrayList<Animal>();

        animalist.add(duck);
        animalist.add(fish);
        animalist.add(zebra);

        for(Animal animal : animalist){
            System.out.println(animal.toString());
        }
    }
}
